# Front End Training - Solution repository

Here you can find all the solution for all exercises presented during the training.
It also has 

## Useful links and documentation:

### Official documentation
- [VUE Documentation](https://vuejs.org/v2/guide/)
- [Nuxt Documentation](https://nuxtjs.org/docs/2.x/get-started/installation)
- [Nuxt Typescript Documentation](https://typescript.nuxtjs.org/guide/introduction)
- [Nuxt property decorator](https://github.com/nuxt-community/nuxt-property-decorator#decorators-and-helpers)
- [Vuetify Documentation](https://vuetifyjs.com/en/getting-started/installation/)

### Trainings and other resources:
- [Official VUE tutorial](https://www.vuemastery.com/courses/intro-to-vue-js/vue-instance/)
- [Server side vs client side rendering with VUE (video)](https://www.youtube.com/watch?v=skhtIHRa3Rw)
- [SOLID violation in VUE (check repo at the end of the page)](https://itnext.io/https-medium-com-manuustenko-how-to-avoid-solid-principles-violations-in-vue-js-application-1121a0df6197)
- [TDD with VUE](https://frontstuff.io/an-introduction-to-tdd-with-vuejs)
- [Front end test pyramid](https://www.freecodecamp.org/news/the-front-end-test-pyramid-rethink-your-testing-3b343c2bca51/)
- [States machines in VUE](https://frontstuff.io/using-state-machines-in-vuejs-with-xstate)
- [Atomic Design](https://dev.to/miladalizadeh/vue-cli-30-plugin-for-creating-apps-using-atomic-design--storybook-42dk)
- [Progressive Web Apps](https://web.dev/what-are-pwas/)

### VS Code plugins:

- [Vetur](https://marketplace.visualstudio.com/items?itemName=octref.vetur)
- [Eslint](https://marketplace.visualstudio.com/items?itemName=dbaeumer.vscode-eslint)
- [jest](https://marketplace.visualstudio.com/items?itemName=Orta.vscode-jest)
- [jest runner](https://marketplace.visualstudio.com/items?itemName=firsttris.vscode-jest-runner)

## Build Setup

```bash
# install dependencies
$ yarn install

# Server for development purposes with hot reload at localhost:3000
$ yarn dev

# build for production and launch server
$ yarn build
$ yarn start

# generate static project
$ yarn generate
```

For detailed explanation on how things work, check out the [documentation](https://nuxtjs.org).
