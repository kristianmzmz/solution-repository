import { mount, shallowMount } from '@vue/test-utils'
import ItemsList from '~/components/ItemsList.vue'

describe('List component', () => {
  it('should render a list of items', () => {
    const itemsListComponent = mount(ItemsList,
      {
        propsData: {
          mountains: [
            { title: 'a mountain' },
            { title: 'an mountain' },
            { title: 'an awesome mountain' }
          ]
        }
      })
    expect(itemsListComponent.findAll('.v-list-item').length).toBe(3)
  })

  it('should mock and render a list of items ', () => {
    const itemsListComponent = shallowMount(ItemsList,
      {
        propsData: {
          mountains: [
            { title: 'a mountain' },
            { title: 'an mountain' },
            { title: 'an awesome mountain' }
          ]
        }
      })
    expect(itemsListComponent.findAll('v-list-item-stub').length).toBe(3)
  })
})
