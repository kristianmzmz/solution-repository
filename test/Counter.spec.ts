import { mount } from '@vue/test-utils'
import Counter from '~/components/Counter.vue'
import CounterNoClassBased from '~/components/CounterNoClassBased.vue'

describe('Counter component implementations', () => {
  describe('Counter component behaviour', () => {
    it('should render the mountains counter', () => {
      const counterComponent = mount(Counter, {
        propsData: {
          itemsNumber: 2
        }
      })
      expect(counterComponent.html()).toContain('Mountains(2)')
    })
    it('should render a zero counter when no props are available', () => {
      const counterComponent = mount(Counter)
      expect(counterComponent.html()).toContain('Mountains(0)')
    })
  })

  describe('Counter No class based behaviour', () => {
    it('should render the mountains counter', () => {
      const counterComponent = mount(CounterNoClassBased, {
        data () {
          return {
            mountains: [
              { title: 'a mountain' }
            ]
          }
        }
      })
      expect(counterComponent.html()).toContain('Mountains(1)')
    })
    it('should render a zero counter when no props are available', () => {
      const counterComponent = mount(CounterNoClassBased)
      expect(counterComponent.html()).toContain('Mountains(0)')
    })
  })
})
