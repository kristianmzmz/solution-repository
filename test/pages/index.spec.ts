import { mount, shallowMount } from '@vue/test-utils'
import IndexPage from '~/pages/index.vue'
import Counter from '~/components/Counter.vue'
import CounterNoClassBased from '~/components/CounterNoClassBased.vue'
import ItemsList from '~/components/ItemsList.vue'

describe('Index page', () => {
  it('should render the page as designed', () => {
    const indexPageWrapper = mount(IndexPage)
    expect(indexPageWrapper.html()).toMatchSnapshot()
  })

  it('should render all the components', () => {
    const indexPageWrapper = shallowMount(IndexPage)
    expect(indexPageWrapper.findComponent(Counter)).toBeTruthy()
    expect(indexPageWrapper.findComponent(CounterNoClassBased)).toBeTruthy()
    expect(indexPageWrapper.findComponent(ItemsList)).toBeTruthy()
  })
})
