import axios from 'axios'

const DEFAULT_API_URL = 'https://api.nuxtjs.dev'

export class ApiClient {
  private apiURL: string = ''
  private path: string = '/mountains'

  constructor (apiURL: string) {
    this.apiURL = apiURL
  }

  async getMountains (): Promise<any[]> {
    return await fetch(
      this.apiURL + this.path
    ).then(res => res.json())
  }

  async getMountain (): Promise<any[]> {
    const allMountains = await axios
      .get(this.apiURL + this.path)
      .then(res => Promise.resolve(res.data))

    return allMountains[0]
  }
}

declare module 'vue/types/vue' {
  interface Vue {
    $apiClient: ApiClient
  }
}

// @ts-ignore
// eslint-disable-next-line @typescript-eslint/no-unused-vars
const apiClient: Plugin = (context, inject) => {
  const apiClient: ApiClient = new ApiClient(DEFAULT_API_URL)

  inject('apiClient', apiClient)
}

export default apiClient
